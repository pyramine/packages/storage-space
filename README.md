# Storage-Space

This packages provides a way to predefine the use `localStorage` and `sessionStorage` keys for your application. It also
provides some additional type-safety for serializing values to the storage engine.

## Installation

```bash
npm install @pyramine/storage-space
```

## Usage

First we will define our spaces:

```ts
import {sessionSpace, localSpace} from "@pyramine/storage-space";

export const storage = {
    first: await sessionSpace<string>('first').initialize('inital-value'),
    second: localSpace<{ complex: string }>('second'),
};


await storage.first.set('myValue');
await storage.second.set({complex: 'myValue'});

await storage.first.get(); // 'myValue'
await storage.second.get(); // { complex: 'myValue' }
```

### Serializers

This package provides some default serializers:

- `JsonSerializer` (used by default)
- `Base64Serializer`

You can switch the serializer at any given time:

```ts
import {Base64Serializer} from "@pyramine/storage-space";

storage.first.serializeUsing(new Base64Serializer());
```

> You can even define custom serializers by creating class which implements our `StorageSerializer` interface.

### Events

The space emit events you can listen on:

| name   | description                                                                   |
|--------|-------------------------------------------------------------------------------|
| change | Fired when ever the underlying value change. Even when the value got removed. |
| remove | Similar to `change` but only fires if the value got removed.                  |

```ts
storage.first.on('change', (event) => {
    console.log(event);
});
storage.first.once('change', (event) => {
    console.log(event);
});
```
