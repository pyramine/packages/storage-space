import { StorageSpace } from '../StorageSpace';
import { MemoryStorage } from '../storages';
import {
    StorageSpaceChangeEvent,
    StorageSpaceRemoveEvent,
} from '../StorageSpaceEvents';
import { Base64Serializer, JsonSerializer } from '../serializer';

it('should set value', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    await storageSpace.set('myValue');

    expect(storage.getItem('test-key')).toBe('{"value":"myValue"}');
});

describe('assign with boolean to value', () => {
    it('should set true', async () => {
        const storage = new MemoryStorage();
        const storageSpace = new StorageSpace<boolean>('test-key', storage);

        await storageSpace.set(true);

        expect(storage.getItem('test-key')).toBe('{"value":true}');
    });

    it('should set false', async () => {
        const storage = new MemoryStorage();
        const storageSpace = new StorageSpace<boolean>('test-key', storage);

        await storageSpace.set(false);

        expect(storage.getItem('test-key')).toBe('{"value":false}');
    });
});

it('should serialize the values', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace<Record<string, string>>(
        'test-key',
        storage,
    );

    await storageSpace.set({ myKey: 'myValue' });

    expect(storage.getItem('test-key')).toBe('{"value":{"myKey":"myValue"}}');
});

it('should get value after setting', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    await storageSpace.set('myValue');

    expect(await storageSpace.get()).toBe('myValue');
});

it('should return value', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    await storageSpace.set('myValue');

    expect(await storageSpace.get()).toBe('myValue');
});

it('should return default if value not set', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    expect(await storageSpace.get('myDefault')).toBe('myDefault');
});

it('should return null if value not set and no default provided', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    expect(await storageSpace.get()).toBeNull();
});

it('should return null if value not set and no default provided', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    expect(await storageSpace.get()).toBeNull();
});

it('should remove item', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    await storageSpace.set('myValue');

    expect(await storageSpace.get()).toBe('myValue');

    await storageSpace.remove();

    expect(await storageSpace.get()).toBeNull();
});

it('should remove item if null passed to value', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    await storageSpace.set('myValue');

    expect(await storageSpace.get()).toBe('myValue');

    await storageSpace.set(null);

    expect(await storageSpace.get()).toBeNull();
});

it('should provide the key', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    expect(storageSpace.key).toBe('test-key');
});

it('should provide the passed storage', async () => {
    const storage = new MemoryStorage();
    const storageSpace = new StorageSpace('test-key', storage);

    expect(storageSpace.storage).toBe(storage);
});

describe('serializer', () => {
    it('should serialize old value with new serializer', async () => {
        const storage = new MemoryStorage();
        const storageSpace = new StorageSpace('test-key', storage);

        await storageSpace.set('myValue');
        expect(storage.getItem('test-key')).toMatchInlineSnapshot(
            `"{"value":"myValue"}"`,
        );

        await storageSpace.serializeUsing(new Base64Serializer());
        expect(storage.getItem('test-key')).toMatchInlineSnapshot(
            `"eyJ2YWx1ZSI6Im15VmFsdWUifQ=="`,
        );
        expect(await storageSpace.get()).toBe('myValue');
    });

    it('should serialize old value with new serializer (boolean)', async () => {
        const storage = new MemoryStorage();
        const storageSpace = new StorageSpace<boolean>('test-key', storage);

        await storageSpace.set(false);
        expect(storage.getItem('test-key')).toMatchInlineSnapshot(
            `"{"value":false}"`,
        );

        await storageSpace.serializeUsing(new Base64Serializer());
        expect(storage.getItem('test-key')).toMatchInlineSnapshot(
            `"eyJ2YWx1ZSI6ZmFsc2V9"`,
        );
        expect(await storageSpace.get()).toBe(false);
    });

    it('should provide the current serializer', async () => {
        const storage = new MemoryStorage();
        const storageSpace = new StorageSpace('test-key', storage);

        expect(storageSpace.serializer).toBeInstanceOf(JsonSerializer);

        const newSerializer = new JsonSerializer();
        expect(storageSpace.serializer).not.toBe(newSerializer);
        await storageSpace.serializeUsing(newSerializer);
        expect(storageSpace.serializer).toBe(newSerializer);
    });
});

describe('copyFrom', () => {
    it('should copy if value is not set', async () => {
        const storage = new MemoryStorage();

        const storageSpace1 = new StorageSpace('test-key-1', storage);
        const storageSpace2 = new StorageSpace('test-key-2', storage);

        await storageSpace1.set('myValue1');

        expect(await storageSpace1.get()).toBe('myValue1');
        expect(await storageSpace2.get()).toBeNull();

        await storageSpace2.copyFrom(storageSpace1);

        expect(await storageSpace1.get()).toBe('myValue1');
        expect(await storageSpace2.get()).toBe('myValue1');
    });

    it('should not copy if value is set', async () => {
        const storage = new MemoryStorage();

        const storageSpace1 = new StorageSpace('test-key-1', storage);
        const storageSpace2 = new StorageSpace('test-key-2', storage);

        await storageSpace1.set('myValue1');
        await storageSpace2.set('myValue2');

        expect(await storageSpace1.get()).toBe('myValue1');
        expect(await storageSpace2.get()).toBe('myValue2');

        await storageSpace2.copyFrom(storageSpace1);

        expect(await storageSpace1.get()).toBe('myValue1');
        expect(await storageSpace2.get()).toBe('myValue2');
    });

    it('should copy and overwrite if value is set', async () => {
        const storage = new MemoryStorage();

        const storageSpace1 = new StorageSpace('test-key-1', storage);
        const storageSpace2 = new StorageSpace('test-key-2', storage);

        await storageSpace1.set('myValue1');
        await storageSpace2.set('myValue2');

        expect(await storageSpace1.get()).toBe('myValue1');
        expect(await storageSpace2.get()).toBe('myValue2');

        await storageSpace2.copyFrom(storageSpace1, true);

        expect(await storageSpace1.get()).toBe('myValue1');
        expect(await storageSpace2.get()).toBe('myValue1');
    });

    it('should copy and remove if value is set', async () => {
        const storage = new MemoryStorage();

        const storageSpace1 = new StorageSpace('test-key-1', storage);
        const storageSpace2 = new StorageSpace('test-key-2', storage);

        await storageSpace2.set('myValue2');

        expect(await storageSpace1.get()).toBeNull();
        expect(await storageSpace2.get()).toBe('myValue2');

        await storageSpace2.copyFrom(storageSpace1, true);

        expect(await storageSpace1.get()).toBeNull();
        expect(await storageSpace2.get()).toBeNull();
    });
});

describe('initial value', () => {
    it('should set the initial value if storage is empty', async () => {
        const storage = new MemoryStorage();
        const storageSpace = await new StorageSpace(
            'test-key',
            storage,
        ).initialize('initial value');

        expect(await storageSpace.get()).toBe('initial value');
    });

    it('should not set the initial value if storage already has an value', async () => {
        const storage = new MemoryStorage();
        storage.setItem('test-key', '{"value":"my Value"}');
        const storageSpace = await new StorageSpace(
            'test-key',
            storage,
        ).initialize('initial value');

        expect(await storageSpace.get()).toBe('my Value');
    });
});

describe('event system', () => {
    it('should emit "change" event', async () => {
        const storage = new MemoryStorage();
        const storageSpace = new StorageSpace('test-key', storage);

        const eventCallback: (event: StorageSpaceChangeEvent<string>) => void =
            jest.fn();
        storageSpace.on('change', eventCallback);
        expect(eventCallback).not.toHaveBeenCalled();

        await storageSpace.set('new value');

        expect(eventCallback).toHaveBeenCalledWith({
            space: storageSpace,
            newValue: 'new value',
            oldValue: null,
        });
    });

    it('should emit "remove" event without previous value', async () => {
        const storage = new MemoryStorage();
        const storageSpace = new StorageSpace('test-key', storage);

        const eventCallback: (event: StorageSpaceRemoveEvent<string>) => void =
            jest.fn();
        storageSpace.on('remove', eventCallback);
        expect(eventCallback).not.toHaveBeenCalled();

        await storageSpace.remove();

        expect(eventCallback).toHaveBeenCalledWith({
            space: storageSpace,
            oldValue: null,
        });
    });

    it('should emit "remove" event with previous value', async () => {
        const storage = new MemoryStorage();
        const storageSpace = await new StorageSpace(
            'test-key',
            storage,
        ).initialize('my value');

        const eventCallback: (event: StorageSpaceRemoveEvent<string>) => void =
            jest.fn();
        storageSpace.on('remove', eventCallback);
        expect(eventCallback).not.toHaveBeenCalled();

        await storageSpace.remove();

        expect(eventCallback).toHaveBeenCalledWith({
            space: storageSpace,
            oldValue: 'my value',
        });
    });

    it('should stop listening for events', async () => {
        const storage = new MemoryStorage();
        const storageSpace = await new StorageSpace(
            'test-key',
            storage,
        ).initialize('my value');

        const eventCallback: (event: StorageSpaceChangeEvent<string>) => void =
            jest.fn();
        storageSpace.on('change', eventCallback);
        expect(eventCallback).not.toHaveBeenCalled();

        await storageSpace.set('new Value');
        expect(eventCallback).toHaveBeenCalledTimes(1);
        expect(eventCallback).toHaveBeenCalledWith({
            space: storageSpace,
            oldValue: 'my value',
            newValue: 'new Value',
        });

        storageSpace.off('change', eventCallback);

        await storageSpace.set('new Value 2');
        expect(eventCallback).toHaveBeenCalledTimes(1);
    });

    it('should listen for events once', async () => {
        const storage = new MemoryStorage();
        const storageSpace = await new StorageSpace(
            'test-key',
            storage,
        ).initialize('my value');

        const eventCallback: (event: StorageSpaceChangeEvent<string>) => void =
            jest.fn();
        storageSpace.once('change', eventCallback);
        expect(eventCallback).not.toHaveBeenCalled();

        await storageSpace.set('new Value');
        expect(eventCallback).toHaveBeenCalledTimes(1);
        expect(eventCallback).toHaveBeenCalledWith({
            space: storageSpace,
            oldValue: 'my value',
            newValue: 'new Value',
        });

        await storageSpace.set('new Value 2');
        expect(eventCallback).toHaveBeenCalledTimes(1);
    });
});
