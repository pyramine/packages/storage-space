import * as index from '../index';

it('should provide consistent api', () => {
    expect(index).toMatchInlineSnapshot(`
        {
          "Base64Serializer": [Function],
          "JsonSerializer": [Function],
          "LocalStorageSpace": [Function],
          "MemoryStorage": [Function],
          "MemoryStorageSpace": [Function],
          "SessionStorageSpace": [Function],
          "StorageSpace": [Function],
          "localSpace": [Function],
          "memorySpace": [Function],
          "memoryStorage": MemoryStorage {
            "_storage": Map {},
          },
          "sessionSpace": [Function],
        }
    `);
});
