import { MemoryStorage } from '../MemoryStorage';

it('should store values', () => {
    const storage = new MemoryStorage();

    storage.setItem('myKey', 'myValue');
    expect(storage.getItem('myKey')).toBe('myValue');
});

it('should return null if key does not exist', () => {
    const storage = new MemoryStorage();

    expect(storage.getItem('non-existent')).toBeNull();
});

it('should remove a value', () => {
    const storage = new MemoryStorage();

    storage.setItem('myKey1', 'myValue1');
    storage.setItem('myKey2', 'myValue2');

    expect(storage.getItem('myKey1')).toBe('myValue1');
    expect(storage.getItem('myKey2')).toBe('myValue2');

    storage.removeItem('myKey2');

    expect(storage.getItem('myKey1')).toBe('myValue1');
    expect(storage.getItem('myKey2')).toBeNull();
});

it('should clear all values', () => {
    const storage = new MemoryStorage();

    storage.setItem('myKey1', 'myValue1');
    storage.setItem('myKey2', 'myValue2');

    expect(storage.getItem('myKey1')).toBe('myValue1');
    expect(storage.getItem('myKey2')).toBe('myValue2');

    storage.clear();

    expect(storage.getItem('myKey1')).toBeNull();
    expect(storage.getItem('myKey2')).toBeNull();
});

it('should provide length', () => {
    const storage = new MemoryStorage();

    expect(storage.length).toBe(0);

    storage.setItem('myKey1', 'myValue1');
    storage.setItem('myKey2', 'myValue2');

    expect(storage.length).toBe(2);

    storage.clear();

    expect(storage.length).toBe(0);
});

it('should provide key for index', () => {
    const storage = new MemoryStorage();

    storage.setItem('myKey1', 'myValue1');
    storage.setItem('myKey2', 'myValue2');

    expect(storage.key(0)).toBe('myKey1');
    expect(storage.key(1)).toBe('myKey2');
});
