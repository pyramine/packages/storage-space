/**
 * A storage implementation which uses a JavaScript {@link Map} to store its values
 * @public
 */
export class MemoryStorage implements Storage {
    private readonly _storage = new Map<string, string>();

    /**
     * Removes all key/value pairs, if there are any.
     *
     * Dispatches a storage event on Window objects holding an equivalent Storage object.
     */
    clear(): void {
        this._storage.clear();
    }

    /**
     * Returns the current value associated with the given key, or null if the given key does not exist.
     * @param key - the key to lookup
     */
    getItem(key: string): string | null {
        return this._storage.get(String(key)) ?? null;
    }

    /**
     * Removes the key/value pair with the given key, if a key/value pair with the given key exists.
     *
     * Dispatches a storage event on Window objects holding an equivalent Storage object.
     * @param key - the key to remove
     */
    removeItem(key: string): void {
        this._storage.delete(String(key));
    }

    /**
     * Returns the name of the nth key, or null if n is greater than or equal to the number of key/value pairs.
     * @param index - index to lookup
     */
    key(index: number): string | null {
        return [...this._storage.keys()][Number(index)] ?? null;
    }

    /**
     * Sets the value of the pair identified by key to value, creating a new key/value pair if none existed for key previously.
     *
     * Throws a "QuotaExceededError" DOMException exception if the new value couldn't be set. (Setting could fail if, e.g., the user has disabled storage for the site, or if the quota has been exceeded.)
     *
     * Dispatches a storage event on Window objects holding an equivalent Storage object.
     * @param key - the key to set/overwrite
     * @param value - the value to save to the storage
     */
    setItem(key: string, value: string): void {
        this._storage.set(String(key), String(value));
    }

    /**
     * Returns the number of key/value pairs.
     */
    get length(): number {
        return this._storage.size;
    }
}

/**
 * @public
 * This is a shared Storage implementation which is reset after each page refresh
 */
export const memoryStorage = new MemoryStorage();
