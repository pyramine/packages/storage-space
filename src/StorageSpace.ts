import type { StorageSerializer } from './serializer';
import { JsonSerializer } from './serializer';
import { StorageSpaceEvents } from './StorageSpaceEvents';
import { EventHub, EventListener } from '@pyramine/events';
import { Storage } from './utils';

/**
 * @public
 */
export type PrimitiveTypes = string | number | boolean | null | bigint;

/**
 * @public
 */
export type Serializable<T> = {
    [key in keyof T]: PrimitiveTypes | Serializable<T[key]>;
};

/**
 * A utility to provide predefined and constistent access to specific keys inside a {@link Storage}
 * @public
 */
export class StorageSpace<ValueType extends Serializable<ValueType> = string> {
    private readonly _key: string;
    private readonly _storage: Storage;
    private readonly _eventHub = new EventHub<StorageSpaceEvents<ValueType>>();
    private _serializer: StorageSerializer<ValueType> =
        new JsonSerializer<ValueType>();

    /**
     * create a new space for a given key
     * @param key - the key used to save and retrieve data from the storage
     * @param storage - the engine to store and retrieve values from
     */
    constructor(key: string, storage: Storage) {
        this._key = key;
        this._storage = storage;
    }

    /**
     * Initialize storage space with value if it is empty
     * @param initialValue - if the storage is empty for this key, set an initial value
     */
    public async initialize(
        initialValue: ValueType,
    ): Promise<StorageSpace<ValueType>> {
        if (!(await this.get())) {
            await this.set(initialValue);
        }

        return this;
    }

    /**
     * get the used key for this space
     */
    get key(): string {
        return this._key;
    }

    /**
     * get the underlying storage engine used by this space.
     */
    get storage(): Storage {
        return this._storage;
    }

    /**
     * get the current serializer used for this space.
     */
    get serializer(): StorageSerializer<ValueType> {
        return this._serializer;
    }

    /**
     * get the saved value. null if nothing is stored
     */
    async get(): Promise<ValueType | null>;
    /**
     * get the saved value. if nothing is stored {@link otherwise} is returned
     * @param otherwise - the default value to use if nothing is stored.
     */
    async get(otherwise: ValueType): Promise<ValueType>;
    async get(otherwise?: ValueType): Promise<ValueType | null> {
        const data: string | null = await this._storage.getItem(this._key);
        if (data !== null) {
            return this.deserializeValue(data);
        }

        if (otherwise) {
            return otherwise;
        }

        return null;
    }

    /**
     * save a value associate by the {@link StorageSpace.key} inside the storage
     * @param value - value to set in the storage
     */
    async set(value: ValueType | undefined | null): Promise<void> {
        if (!(value !== null && value !== undefined)) {
            return this.remove();
        }

        const oldValue = await this.get();
        await this._storage.setItem(
            this._key,
            await this.serializeValue(value),
        );
        this._eventHub.emit('change', {
            space: this,
            newValue: value,
            oldValue,
        });
    }

    /**
     * copy a value from another {@link StorageSpace}
     * @param from - the space to copy the value from
     * @param overwrite - whether you want to overwrite an existing value
     */
    async copyFrom<O extends ValueType>(
        from: StorageSpace<O>,
        overwrite = false,
    ) {
        const value = await this.get();
        if (!overwrite && value) {
            return;
        }

        const newValue = await from.get();
        if (newValue) {
            await this.set(newValue);
        } else {
            await this.remove();
        }
    }

    /**
     * removes a saved value. if nothing was stored, nothing will happen.
     * After calling this method {@link StorageSpace.get} will return null until a new value is saved.
     */
    async remove(): Promise<void> {
        const oldValue = await this.get();
        await this._storage.removeItem(this._key);
        this._eventHub.emit('remove', { space: this, oldValue });
        this._eventHub.emit('change', {
            space: this,
            oldValue,
            newValue: null,
        });
    }

    /**
     * serialize a given value to its string representation
     * @param value - value to stringify
     * @protected
     */
    protected serializeValue(value: ValueType): string | Promise<string> {
        return this._serializer.serialize(value);
    }

    /**
     * deserialize a given string to its object-like representation
     * @param data - string to parse
     * @protected
     */
    protected deserializeValue(data: string): Promise<ValueType> | ValueType {
        return this._serializer.deserialize(data);
    }

    /**
     * change the used Serializer to serialize the value to the storage
     * You can also dynamically change the serializer. The value will be serialized by the new serializer
     * @param serializer - the serializer to use from now on
     */
    public async serializeUsing(
        serializer: StorageSerializer<ValueType>,
    ): Promise<this> {
        // deserialize value with old serializer and serialize with new one. This way we could change the serializer dynamically.
        const value = await this.get();
        this._serializer = serializer;
        if (value !== null && value !== undefined) {
            await this._storage.setItem(
                this._key,
                await this.serializeValue(value),
            );
        }

        return this;
    }

    /**
     * @internal
     */
    protected get eventHub() {
        return this._eventHub;
    }

    /**
     * listen for a specified event. The callback will be called when the event occurs
     * @param event - the event's name to listen for
     * @param callback - the callback to call when the event occurs
     */
    public on<EventName extends keyof StorageSpaceEvents<ValueType>>(
        event: EventName,
        callback: EventListener<StorageSpaceEvents<ValueType>, EventName>,
    ): void {
        return this._eventHub.on(event, callback);
    }

    /**
     * listen for a specified event only once. the callback is turned off when it was called.
     * @param event - the event's name to listen for
     * @param callback - the callback to call _once_ when the event occurs
     */
    public once<EventName extends keyof StorageSpaceEvents<ValueType>>(
        event: EventName,
        callback: EventListener<StorageSpaceEvents<ValueType>, EventName>,
    ): void {
        return this._eventHub.once(event, callback);
    }

    /**
     * stop listening for new events
     * @param event - the event's name to stop listening for
     * @param callback - the callback to stop calling.
     */
    public off<EventName extends keyof StorageSpaceEvents<ValueType>>(
        event: EventName,
        callback: EventListener<StorageSpaceEvents<ValueType>, EventName>,
    ): void {
        return this._eventHub.off(event, callback);
    }
}
