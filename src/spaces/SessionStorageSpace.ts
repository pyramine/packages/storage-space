import { Serializable, StorageSpace } from '../StorageSpace';
import { registerStorageEventListener } from '../utils/handleStorageEvent';
import { memoryStorage } from '../storages';

/**
 * a {@link StorageSpace} implementation using the {@link sessionStorage}
 * @public
 */
export class SessionStorageSpace<
    T extends Serializable<T>,
> extends StorageSpace<T> {
    constructor(key: string) {
        super(key, SessionStorageSpace.getStorageEngine());

        registerStorageEventListener(this);
    }

    private static getStorageEngine() {
        return typeof sessionStorage !== 'undefined'
            ? sessionStorage
            : memoryStorage;
    }
}

/**
 * create a new {@link SessionStorageSpace}
 * @param key - the key used to save and retrieve data from the storage
 * @public
 */
export function sessionSpace<T extends Serializable<T>>(
    key: string,
): SessionStorageSpace<T> {
    return new SessionStorageSpace<T>(key);
}
