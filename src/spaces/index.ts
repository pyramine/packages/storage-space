export { LocalStorageSpace, localSpace } from './LocalStorageSpace';
export { MemoryStorageSpace, memorySpace } from './MemoryStorageSpace';
export { SessionStorageSpace, sessionSpace } from './SessionStorageSpace';
