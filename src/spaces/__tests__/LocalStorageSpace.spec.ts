import { StorageSpace } from '../../StorageSpace';
import { localSpace, LocalStorageSpace } from '../LocalStorageSpace';
import { memoryStorage, MemoryStorage } from '../../storages';

describe('localStorage available (browser)', () => {
    beforeAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        global.localStorage = new MemoryStorage();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        delete global.localStorage;
    });

    it('should instantiate', () => {
        expect(new LocalStorageSpace('myTestKey')).toBeInstanceOf(StorageSpace);
    });

    it('should instantiate through helper', () => {
        expect(localSpace('dummy-key')).toBeInstanceOf(LocalStorageSpace);
    });

    it('should use localStorage', () => {
        expect(localSpace('dummy-key').storage).toBeInstanceOf(MemoryStorage);
    });
});

describe('localStorage not available (node.js)', () => {
    it('should instantiate', () => {
        expect(new LocalStorageSpace('myTestKey')).toBeInstanceOf(StorageSpace);
    });

    it('should instantiate through helper', () => {
        expect(localSpace('dummy-key')).toBeInstanceOf(LocalStorageSpace);
    });

    it('should fallback to MemoryStorage', () => {
        expect(localSpace('dummy-key').storage).toBeInstanceOf(MemoryStorage);
        expect(localSpace('dummy-key').storage).toBe(memoryStorage);
    });
});
