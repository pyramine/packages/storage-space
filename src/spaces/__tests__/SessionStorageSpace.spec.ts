import { StorageSpace } from '../../StorageSpace';
import { sessionSpace, SessionStorageSpace } from '../SessionStorageSpace';
import { memoryStorage, MemoryStorage } from '../../storages';

describe('sessionStorage available (browser)', () => {
    beforeAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        global.sessionStorage = new MemoryStorage();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        delete global.sessionStorage;
    });

    it('should instantiate', () => {
        expect(new SessionStorageSpace('myTestKey')).toBeInstanceOf(
            StorageSpace,
        );
    });

    it('should instantiate through helper', () => {
        expect(sessionSpace('dummy-key')).toBeInstanceOf(SessionStorageSpace);
    });

    it('should use sessionStorage', () => {
        expect(sessionSpace('dummy-key').storage).toBeInstanceOf(MemoryStorage);
    });
});

describe('sessionStorage not available (node.js)', () => {
    it('should instantiate', () => {
        expect(new SessionStorageSpace('myTestKey')).toBeInstanceOf(
            StorageSpace,
        );
    });

    it('should instantiate through helper', () => {
        expect(sessionSpace('dummy-key')).toBeInstanceOf(SessionStorageSpace);
    });

    it('should fallback to MemoryStorage', () => {
        expect(sessionSpace('dummy-key').storage).toBeInstanceOf(MemoryStorage);
        expect(sessionSpace('dummy-key').storage).toBe(memoryStorage);
    });
});
