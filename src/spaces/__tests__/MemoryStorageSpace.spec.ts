import { StorageSpace } from '../../StorageSpace';
import { MemoryStorageSpace, memorySpace } from '../MemoryStorageSpace';

it('should instantiate', () => {
    expect(new MemoryStorageSpace('myTestKey')).toBeInstanceOf(StorageSpace);
});

it('should instantiate through helper', () => {
    expect(memorySpace('dummy-key')).toBeInstanceOf(MemoryStorageSpace);
});
