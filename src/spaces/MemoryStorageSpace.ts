import { Serializable, StorageSpace } from '../StorageSpace';
import { memoryStorage } from '../storages';

/**
 * a {@link StorageSpace} implementation using the {@link memoryStorage}
 * @public
 */
export class MemoryStorageSpace<
    T extends Serializable<T>,
> extends StorageSpace<T> {
    constructor(key: string) {
        super(key, memoryStorage);
    }
}

/**
 * create a new {@link MemoryStorageSpace}
 * @param key - the key used to save and retrieve data from the storage
 * @public
 */
export function memorySpace<T extends Serializable<T>>(
    key: string,
): MemoryStorageSpace<T> {
    return new MemoryStorageSpace<T>(key);
}
