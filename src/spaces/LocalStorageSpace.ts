import { Serializable, StorageSpace } from '../StorageSpace';
import { registerStorageEventListener } from '../utils/handleStorageEvent';
import { memoryStorage } from '../storages';

/**
 * a {@link StorageSpace} implementation using the {@link localStorage}
 * @public
 */
export class LocalStorageSpace<
    T extends Serializable<T>,
> extends StorageSpace<T> {
    constructor(key: string) {
        super(key, LocalStorageSpace.getStorageEngine());

        registerStorageEventListener(this);
    }

    private static getStorageEngine() {
        return typeof localStorage !== 'undefined'
            ? localStorage
            : memoryStorage;
    }
}

/**
 * create a new {@link LocalStorageSpace}
 * @param key - the key used to save and retrieve data from the storage
 * @public
 */
export function localSpace<T extends Serializable<T>>(
    key: string,
): LocalStorageSpace<T> {
    return new LocalStorageSpace<T>(key);
}
