import type { EventMap } from '@pyramine/events';
import type { StorageSpace } from './StorageSpace';

/**
 * The most basic attributes all StorageSpace events should have
 * @public
 */
export interface StorageSpaceEvent<ValueType> {
    space: StorageSpace<ValueType>;
}

/**
 * This event occurs when the value of a storage space changes
 * @public
 */
export interface StorageSpaceChangeEvent<ValueType>
    extends StorageSpaceEvent<ValueType> {
    oldValue: ValueType | null;
    newValue: ValueType | null;
}

/**
 * This event occurs when a value gets removed from a storage space
 * @public
 */
export interface StorageSpaceRemoveEvent<ValueType>
    extends StorageSpaceEvent<ValueType> {
    oldValue: ValueType | null;
}

/**
 * All possible events that can occur on StorageSpaces
 * @public
 */
export interface StorageSpaceEvents<ValueType> extends EventMap {
    change: [event: StorageSpaceChangeEvent<ValueType>];
    remove: [event: StorageSpaceRemoveEvent<ValueType>];
}
