import { Serializable, StorageSpace } from '../StorageSpace';
import { StorageEvent } from './StorageEvent';

/**
 * handle a storage event triggered by another tab/window
 * @param event - the event to handle
 * @internal
 */
export function handleStorageEvent(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this: StorageSpace<any>,
    event: StorageEvent,
): void {
    if (
        (event.key && event.key !== this.key) ||
        event.storageArea !== this.storage
    ) {
        return;
    }

    this.eventHub.emit('change', {
        space: this,
        newValue: event.newValue ? this.deserializeValue(event.newValue) : null,
        oldValue: event.oldValue ? this.deserializeValue(event.oldValue) : null,
    });
}

/**
 * register a new event listener on the window object to handle storage events from other tabs/windows
 * @param space - the storage space to register the listener on
 * @internal
 */
export function registerStorageEventListener<T extends Serializable<T>>(
    space: StorageSpace<T>,
) {
    if (typeof window === 'undefined') {
        return;
    }

    window.addEventListener('storage', handleStorageEvent.bind(space));
}
