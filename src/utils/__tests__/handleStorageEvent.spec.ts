/** @jest-environment jsdom */
import { MemoryStorageSpace } from '../../spaces';
import {
    handleStorageEvent,
    registerStorageEventListener,
} from '../handleStorageEvent';
import { MemoryStorage } from '../../storages';
import { StorageEvent } from '../StorageEvent';

function createStorageEvent(options: Partial<StorageEvent>): StorageEvent {
    return {
        newValue: options.newValue || null,
        oldValue: options.oldValue || null,
        key: options.key || null,
        storageArea: options.storageArea || null,
    };
}

describe('handleStorageEvent', () => {
    it('should emit a change event', () => {
        const emitMock = jest.fn();
        const space = new MemoryStorageSpace('test');
        space.on('change', emitMock);
        handleStorageEvent.call(
            space,
            createStorageEvent({
                key: 'test',
                oldValue: '{"value":"old"}',
                newValue: '{"value":"new"}',
                storageArea: space.storage,
            }),
        );

        expect(emitMock).toHaveBeenCalled();
        expect(emitMock).toHaveBeenCalledWith({
            newValue: 'new',
            oldValue: 'old',
            space: space,
        });
    });

    it('should default to null', () => {
        const emitMock = jest.fn();
        const space = new MemoryStorageSpace('test');
        space.on('change', emitMock);
        handleStorageEvent.call(
            space,
            createStorageEvent({
                key: 'test',
                storageArea: space.storage,
            }),
        );

        expect(emitMock).toHaveBeenCalled();
        expect(emitMock).toHaveBeenCalledWith({
            newValue: null,
            oldValue: null,
            space: space,
        });
    });

    it('should not emit on wrong key', () => {
        const emitMock = jest.fn();
        const space = new MemoryStorageSpace('test');
        space.on('change', emitMock);
        handleStorageEvent.call(
            space,
            createStorageEvent({
                key: 'test2',
                storageArea: space.storage,
            }),
        );

        expect(emitMock).not.toHaveBeenCalledWith();
    });

    it('should not emit on wrong storage', () => {
        const emitMock = jest.fn();
        const space = new MemoryStorageSpace('test');
        space.on('change', emitMock);
        handleStorageEvent.call(
            space,
            createStorageEvent({
                key: 'test2',
                storageArea: new MemoryStorage(),
            }),
        );

        expect(emitMock).not.toHaveBeenCalledWith();
    });

    it('should emit on calling clear (key is null)', () => {
        const emitMock = jest.fn();
        const space = new MemoryStorageSpace('test');
        space.on('change', emitMock);
        handleStorageEvent.call(
            space,
            createStorageEvent({
                storageArea: space.storage,
            }),
        );

        expect(emitMock).toHaveBeenCalled();
        expect(emitMock).toHaveBeenCalledWith({
            newValue: null,
            oldValue: null,
            space: space,
        });
    });
});

xdescribe('registerStorageEventListener', () => {
    it('should register callback and listen for events', () => {
        const emitMock = jest.fn();
        const space = new MemoryStorageSpace('test');
        space.on('change', emitMock);
        registerStorageEventListener(space);

        window.dispatchEvent(
            // @ts-expect-error Custom events do not match browser events
            createStorageEvent({
                key: 'test',
                oldValue: '{"value":"old"}',
                newValue: '{"value":"new"}',
                storageArea: space.storage,
            }),
        );

        expect(emitMock).toHaveBeenCalled();
        expect(emitMock).toHaveBeenCalledWith({
            newValue: 'new',
            oldValue: 'old',
            space: space,
        });
    });
});
