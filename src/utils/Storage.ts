/**
 * @public
 */
export interface Storage {
    /**
     * Removes all key/value pairs, if there are any.
     */
    clear(): void | Promise<void>;

    /**
     * Returns the current value associated with the given key, or null if the given key does not exist.
     */
    getItem(key: string): string | null | Promise<string | null>;

    /**
     * Removes the key/value pair with the given key, if a key/value pair with the given key exists.
     */
    removeItem(key: string): void | Promise<void>;

    /**
     * Sets the value of the pair identified by key to value, creating a new key/value pair if none existed for key previously.
     */
    setItem(key: string, value: string): void | Promise<void>;
}
