import { StorageSerializer } from './StorageSerializer';
import { Serializable } from '../StorageSpace';
import { JsonSerializer } from './JsonSerializer';
import { encode, decode } from 'js-base64';

/**
 * Serialize a value using the native JSON api of JavaScript and then convert
 * it to a base64 encoded string.
 * @public
 */
export class Base64Serializer<T extends Serializable<T> = string>
    implements StorageSerializer<T>
{
    private readonly _jsonSerializer = new JsonSerializer<T>();

    /**
     * Uses JSON.parse to create an object from the json and base63 encoded string.
     * @param storedValue - the provided Json-String
     */
    deserialize(storedValue: string): T {
        return this._jsonSerializer.deserialize(decode(storedValue));
    }

    /**
     * Uses JSON.stringify to create a valid Json-String from the value which
     * then gets base64 encoded
     * @param value - the value to convert into json
     */
    serialize(value: T): string {
        return encode(this._jsonSerializer.serialize(value));
    }
}
