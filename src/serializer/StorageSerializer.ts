import type { Serializable } from '../StorageSpace';

/**
 * @public
 * Defines all methods needed to serialize and deserialize a value from storage
 */
export interface StorageSerializer<T extends Serializable<T> = string> {
    /**
     * This method serializes an arbitrary value to a string representation which can be stored to a StorageSpace
     * @param value - value to serialize
     */
    serialize(value: T): string | Promise<string>;

    /**
     * This method deserialize a string that was serialized with this Serializer which was stored in a StorageSpace
     * @param storedValue - the string representation to deserialize
     */
    deserialize(storedValue: string): T | Promise<T>;
}
