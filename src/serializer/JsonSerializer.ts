import type { StorageSerializer } from './StorageSerializer';
import type { Serializable } from '../StorageSpace';

/**
 * Serialize a value using the native JSON api of JavaScript
 * @public
 */
export class JsonSerializer<T extends Serializable<T> = string>
    implements StorageSerializer<T>
{
    /**
     * Uses JSON.parse to create an object from the json string.
     * @param storedValue - the provided Json-String
     */
    deserialize(storedValue: string): T {
        return (JSON.parse(storedValue) as { value: T }).value;
    }

    /**
     * Uses JSON.stringify to create a valid Json-String from the value
     * @param value - the value to convert into json
     */
    serialize(value: T): string {
        return JSON.stringify({ value });
    }
}
