import { Base64Serializer } from '../Base64Serializer';

describe('serialize', () => {
    it('should serialize a number', () => {
        const serializer = new Base64Serializer<number>();

        expect(serializer.serialize(1)).toMatchInlineSnapshot(
            `"eyJ2YWx1ZSI6MX0="`,
        );
    });

    it('should serialize a string', () => {
        const serializer = new Base64Serializer<string>();

        expect(serializer.serialize('bla')).toMatchInlineSnapshot(
            `"eyJ2YWx1ZSI6ImJsYSJ9"`,
        );
    });

    it('should serialize a boolean', () => {
        const serializer = new Base64Serializer<boolean>();

        expect(serializer.serialize(true)).toMatchInlineSnapshot(
            `"eyJ2YWx1ZSI6dHJ1ZX0="`,
        );
    });

    it('should not serialize a symbol', () => {
        const serializer = new Base64Serializer<symbol>();

        expect(serializer.serialize(Symbol('bla'))).toMatchInlineSnapshot(
            `"e30="`,
        );
    });

    it('should serialize a null', () => {
        const serializer = new Base64Serializer<null>();

        expect(serializer.serialize(null)).toMatchInlineSnapshot(
            `"eyJ2YWx1ZSI6bnVsbH0="`,
        );
    });

    it('should not serialize a undefined', () => {
        const serializer = new Base64Serializer<undefined>();

        expect(serializer.serialize(undefined)).toMatchInlineSnapshot(`"e30="`);
    });
});

describe('deserialize', () => {
    it('should serialize a number', () => {
        const serializer = new Base64Serializer<number>();

        expect(serializer.deserialize('eyJ2YWx1ZSI6MX0=')).toBe(1);
    });

    it('should serialize a string', () => {
        const serializer = new Base64Serializer<string>();

        expect(serializer.deserialize('eyJ2YWx1ZSI6ImJsYSJ9')).toBe('bla');
    });

    it('should serialize a boolean', () => {
        const serializer = new Base64Serializer<boolean>();

        expect(serializer.deserialize('eyJ2YWx1ZSI6dHJ1ZX0=')).toBe(true);
    });

    it('should serialize a null', () => {
        const serializer = new Base64Serializer<null>();

        expect(serializer.deserialize('eyJ2YWx1ZSI6bnVsbH0=')).toBeNull();
    });
});
