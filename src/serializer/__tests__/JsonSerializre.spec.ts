import { JsonSerializer } from '../JsonSerializer';

describe('serialize', () => {
    it('should serialize a number', () => {
        const serializer = new JsonSerializer<number>();

        expect(serializer.serialize(1)).toMatchInlineSnapshot(`"{"value":1}"`);
    });

    it('should serialize a string', () => {
        const serializer = new JsonSerializer<string>();

        expect(serializer.serialize('bla')).toMatchInlineSnapshot(
            `"{"value":"bla"}"`,
        );
    });

    it('should serialize a boolean', () => {
        const serializer = new JsonSerializer<boolean>();

        expect(serializer.serialize(true)).toMatchInlineSnapshot(
            `"{"value":true}"`,
        );
    });

    it('should not serialize a symbol', () => {
        const serializer = new JsonSerializer<symbol>();

        expect(serializer.serialize(Symbol('bla'))).toMatchInlineSnapshot(
            `"{}"`,
        );
    });

    it('should serialize a null', () => {
        const serializer = new JsonSerializer<null>();

        expect(serializer.serialize(null)).toMatchInlineSnapshot(
            `"{"value":null}"`,
        );
    });

    it('should not serialize a undefined', () => {
        const serializer = new JsonSerializer<undefined>();

        expect(serializer.serialize(undefined)).toMatchInlineSnapshot(`"{}"`);
    });
});

describe('deserialize', () => {
    it('should serialize a number', () => {
        const serializer = new JsonSerializer<number>();

        expect(serializer.deserialize('{"value":1}')).toBe(1);
    });

    it('should serialize a string', () => {
        const serializer = new JsonSerializer<string>();

        expect(serializer.deserialize('{"value":"bla"}')).toBe('bla');
    });

    it('should serialize a boolean', () => {
        const serializer = new JsonSerializer<boolean>();

        expect(serializer.deserialize('{"value":true}')).toBe(true);
    });

    it('should serialize a null', () => {
        const serializer = new JsonSerializer<null>();

        expect(serializer.deserialize('{"value":null}')).toBeNull();
    });
});
